# -*- coding: utf-8 -*- 

import os

"""
每次创建新的 markdown 文件后，索引目录不会自动更新。
简单写了个 py 脚本，每次新建 markdown 文件后，或是突遇灵感，或是写了一章小说，记得运行一下这个脚本，更新目录。
"""

def get_file_name(file_dir):
    """
    输入路径，以列表形式输出该目录下的文件名
    去除了"_sidebar.md"和文件夹
    对文件名进行了排序
    
    例:
    目录下，有文件['_sidebar.md', '凡剑.md', '迷宫之灾.md']
    输出['凡剑', '迷宫之灾']
    """
    allfiles_namelist = os.listdir(file_dir)
    files_namelist = [i[:-3] for i in allfiles_namelist if ((i[-2:] == "md") and (i != "_sidebar.md"))]
    files_namelist.sort()
    return(files_namelist)

def rewrite_mdfile(rewrite_file_dir, rewrite_file_name, files_namelist):
    """
    rewrite_file : 需要改写内容的 markdown 文件
    files_namelist : 文件名列表
    
    该模块用于更新 大纲.md 灵感.md
    """
    f = open(rewrite_file_dir, 'w', encoding='utf-8') # 覆盖写入
    f.write("# %s\n"%rewrite_file_name)
    for i in files_namelist:
        f.write("\n## [%s](%s/%s.md)\n"%(i, rewrite_file_name, i))
    f.close
    return

if __name__ == '__main__':
    灵感文件夹_files_namelist = get_file_name("./灵感")
    rewrite_mdfile("./灵感.md", "灵感", 灵感文件夹_files_namelist)
    大纲文件夹_files_namelist = get_file_name("./大纲")
    rewrite_mdfile("./大纲.md", "大纲", 大纲文件夹_files_namelist)